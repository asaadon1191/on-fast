<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('users')->namespace('UserS')->middleware('guest:web')->group(function()
{
    // FORGET PASSWORD
    Route::get('forgetPassword','forgetPasswordController@forgetPasswordUser')->name('forgetPassword.User');
    Route::post('forgetPassword','forgetPasswordController@sendEmail')->name('forgetPassword.post.User');
    Route::get('reset-password/{token}', 'forgetPasswordController@showResetPasswordForm')->name('reset.password.get');
    Route::post('reset-password', 'forgetPasswordController@submitResetPasswordForm')->name('reset.password.post');
    Route::get('/contactUs', 'User\UsersController@contactUs')->name('user.contactUs');
    Route::post('users/login', 'loginController@makeLogin')->name('user.makeLogin');
});


Route::prefix('users')->middleware('auth:web')->group(function()
{
    Route::get('logout','Auth\LoginController@logout')->name('user.logout');
    Route::get('/packageDetailes/{id}', 'User\UsersController@packageDetailes')->name('user.packageDetailes');
    Route::get('/showData', 'User\UsersController@showData')->name('user.showData');
    Route::get('/filter', 'User\UsersController@filter')->name('user.filter');
    Route::get('/profile', 'User\UsersController@profile')->name('user.profile');
    Route::post('/profile/update', 'User\UsersController@updateProfile')->name('user.profile.update');

    Route::get('/index_product', 'User\productsController@index')->name('user.index.product');
    Route::get('/create_product', 'User\productsController@create')->name('user.create.product');
    Route::get('cities/{id}','User\productsController@cities')->name('Cities');
    Route::post('/create_product', 'User\productsController@store')->name('store.create.product');
    Route::get('/edit_product/{id}', 'User\productsController@edit')->name('user.edit.product');
    Route::post('/update_product/{id}', 'User\productsController@update')->name('user.update.product');
    Route::delete('/delete_product/{id}', 'User\productsController@delete')->name('user.delete.product');


});




Route::get('reset/password/{token}','Auth\ResetPasswordController@ResetsPasswords');

