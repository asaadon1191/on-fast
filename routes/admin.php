<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Note That The Prefix [admin] For All Routes In This File

Route::prefix('admin')->group(function()
{
    Route::middleware('auth:admin')->namespace('Admin')->group(function()
{
    Route::get('Dashboard','DashboardController@index')->name('Dashboard');
    Route::post('payMony/','DashboardController@payMony')->name('Dashboard.payMony');
    Route::get('endStatus{id}/','DashboardController@endStatus')->name('Dashboard.endStatus');
     Route::get('restore1{id}/','DashboardController@restore1')->name('Dashboard.restore1');
    Route::get('restore2{id}/','DashboardController@restore2')->name('Dashboard.restore2');
     Route::get('history/','DashboardController@history')->name('Dashboard.history');
    Route::get('/filter', 'DashboardController@filter')->name('Dashboard.filter');
    Route::get('/filter2', 'DashboardController@filter2')->name('Dashboard.filter2');
    Route::get('/notes', 'DashboardController@notes')->name('Dashboard.notes');
    Route::get('logout','LoginController@logout')->name('admin.logout');


    Route::get('/test','DashboardController@test')->name('test');

    // ************************************** START ADMIN ROUTES ************************
    Route::prefix('admins')->group(function()
    {
        Route::get('/','adminsController@index')->name('admins.index');
        Route::post('/store','adminsController@store')->name('admins.store');
        Route::get('/edit/{id}','adminsController@edit')->name('admins.edit');
        Route::PUT('/update/{id}','adminsController@update')->name('admins.update');
        Route::post('/destroy','adminsController@destroy')->name('admins.destroy');
    });
    // ************************************** END ADMIN ROUTES ************************
    // ************************************** START PERMISSIONS ROUTES ************************
    Route::prefix('permissions')->group(function()
    {
        Route::get('/','PermissionsController@index')->name('permissions.index');
        Route::post('/store','PermissionsController@store')->name('permissions.store');
        Route::get('/edit/{id}','PermissionsController@edit')->name('permissions.edit');
        Route::PUT('/update/{id}','PermissionsController@update')->name('permissions.update');
        Route::post('/destroy','PermissionsController@destroy')->name('permissions.destroy');
    });
    // ************************************** END PERMISSIONS ROUTES ************************
    // ************************************** START ROLES ROUTES ************************
    Route::prefix('roles')->group(function()
    {
        Route::get('/','rolesController@index')->name('roles.index');
        Route::post('/store','rolesController@store')->name('roles.store');
        Route::get('/edit/{id}','rolesController@edit')->name('roles.edit');
        Route::PUT('/update/{id}','rolesController@update')->name('roles.update');
        Route::post('/destroy','rolesController@destroy')->name('roles.destroy');
    });
    // ************************************** END ROLES ROUTES ************************
    // ************************************** START PROFILE ROUTES ************************
    Route::prefix('profile')->group(function()
    {
        Route::get('/edit','ProfileController@edit')->name('pro.edit');
        Route::put('/update','ProfileController@update')->name('pro.update');
        Route::get('/change_password','ProfileController@change_password')->name('pro.change_password');
        Route::post('/make_change_password','ProfileController@make_change_password')->name('pro.make_change_password');

    });
    // ************************************** END PROFILE ROUTES ************************
    // ************************************** START SERVANTS ROUTES ************************
    Route::prefix('servants')->group(function()
    {
        Route::get('/','ServantsController@index')->name('servants.index');
        Route::post('/store','ServantsController@store')->name('servants.store');
        Route::get('/edit/{id}','ServantsController@edit')->name('servants.edit');
        Route::post('/update/{id}','ServantsController@update')->name('servants.update');
        Route::post('/destroy','ServantsController@destroy')->name('servants.destroy');
        Route::get('/getSoftDelete','ServantsController@getSoftDelete')->name('servants.getSoftDelete');
        Route::get('/restore','ServantsController@restore')->name('servants.restore');
    });
    // ************************************** END SERVANTS ROUTES ************************
    // ************************************** START ORDER STATUS ROUTES ************************
    Route::prefix('status')->group(function()
    {
        Route::get('/','StatusController@index')->name('status.index');
        Route::post('/store','StatusController@store')->name('status.store');
        Route::get('/edit/{id}','StatusController@edit')->name('status.edit');
        Route::post('/update/{id}','StatusController@update')->name('status.update');
        Route::post('/destroy','StatusController@destroy')->name('status.destroy');
        Route::get('/getSoftDelete','StatusController@getSoftDelete')->name('status.getSoftDelete');
        Route::get('/restore','StatusController@restore')->name('status.restore');
    });
    // ************************************** END ORDER STATUS ROUTES ************************
    // ************************************** START GOVERNORATES ROUTES ************************
    Route::prefix('governorates')->group(function()
    {
        Route::get('/','governoratesController@index')->name('governorates.index');
        Route::post('/store','governoratesController@store')->name('governorates.store');
        Route::get('/edit/{id}','governoratesController@edit')->name('governorates.edit');
        Route::post('/update/{id}','governoratesController@update')->name('governorates.update');
        Route::post('/destroy','governoratesController@destroy')->name('governorates.destroy');
        Route::get('/getSoftDelete','governoratesController@getSoftDelete')->name('governorates.getSoftDelete');
        Route::get('/restore','governoratesController@restore')->name('governorates.restore');
    });
    // ************************************** END GOVERNORATES ROUTES ************************
    // ************************************** START CITIES ROUTES ************************
    Route::prefix('cities')->group(function()
    {
        Route::get('/','CitiesController@index')->name('cities.index');
        Route::post('/store','CitiesController@store')->name('cities.store');
        Route::get('/edit/{id}','CitiesController@edit')->name('cities.edit');
        Route::post('/update/{id}','CitiesController@update')->name('cities.update');
        Route::post('/destroy','CitiesController@destroy')->name('cities.destroy');
        Route::get('/getSoftDelete','CitiesController@getSoftDelete')->name('cities.getSoftDelete');
        Route::get('/restore','CitiesController@restore')->name('cities.restore');
    });
    // ************************************** END CITIES ROUTES ************************
    // ************************************** START SUPPLIERS ROUTES ************************
    Route::prefix('suppliers')->group(function()
    {
        Route::get('/','SuppliersController@index')->name('suppliers.index');
        Route::get('cities/{id}','SuppliersController@cities')->name('Cities');
        Route::post('/store','SuppliersController@store')->name('suppliers.store');
        Route::get('/edit/{id}','SuppliersController@edit')->name('suppliers.edit');
        Route::post('/update/{id}','SuppliersController@update')->name('suppliers.update');
        Route::post('/destroy','SuppliersController@destroy')->name('suppliers.destroy');
        Route::get('/getSoftDelete','SuppliersController@getSoftDelete')->name('suppliers.getSoftDelete');
        Route::get('/restore','SuppliersController@restore')->name('suppliers.restore');
    });
    // ************************************** END SUPPLIERS ROUTES ************************
    // ************************************** START PRODUCTS ROUTES ************************
    Route::prefix('products')->group(function()
    {
        Route::get('/','ProductsController@index')->name('products.index');
        Route::get('cities/{id}','ProductsController@cities')->name('Cities');
        Route::get('show/{id}','ProductsController@show')->name('products.show');
        Route::post('/store','ProductsController@store')->name('products.store');
        Route::get('/edit/{id}','ProductsController@edit')->name('products.edit');
        Route::post('/update/{id}','ProductsController@update')->name('products.update');
        Route::post('/destroy','ProductsController@destroy')->name('products.destroy');
        Route::get('/getSoftDelete','ProductsController@getSoftDelete')->name('products.getSoftDelete');
        Route::get('/restore','ProductsController@restore')->name('products.restore');
        Route::get('/deletedProducts','ProductsController@deletedProducts')->name('products.deletedProducts');

        Route::get('/uncompleateProducts','ProductsController@uncompleateProducts')->name('products.uncompleateProducts');
        Route::get('/compleatedProducts','ProductsController@compleatedProducts')->name('products.compleatedProducts');

        Route::get('/newProducts','ProductsController@newProducts_Index')->name('products.newProducts_Index');
        Route::get('/newProducts_edit/{id}','ProductsController@newProducts_edit')->name('products.newProducts_edit');
        Route::post('/newProducts_update/{id}','ProductsController@newProducts_update')->name('products.newProducts_update');
        Route::post('/newProducts_accept','ProductsController@newProducts_accept')->name('products.newProducts_accept');
    });
    // ************************************** END PRODUCTS ROUTES ************************
    // ************************************** START ORDER DETAILES ROUTES ************************
    Route::prefix('orderDetailes')->group(function()
    {
        Route::get('/create','orderDetailesController@create')->name('orderDetailes.create');
        Route::get('cities/{id}','orderDetailesController@cities')->name('Cities');
        Route::post('search/','orderDetailesController@search')->name('orderDetailes.search');
        Route::post('forceDelete/{id}','orderDetailesController@forceDelete')->name('orderDetailes.forceDelete');  // DELETE ITEMS FROM ORDER DETAILES TABLE IF I DON,T CREATED NEW ORDER
        Route::post('addToCart/','orderDetailesController@addToCart')->name('orderDetailes.addToCart');
        Route::get('submit_new_order/','orderDetailesController@submit_new_order')->name('orderDetailes.submit_new_order');
        Route::post('changeStatus/','orderDetailesController@changeStatus')->name('orderDetailes.changeStatus');
        Route::post('changeShippingPrice/','orderDetailesController@changeShippingPrice')->name('orderDetailes.changeShippingPrice');
        Route::post('deleteProduct/','orderDetailesController@deleteProduct')->name('orderDetailes.deleteProduct');

                Route::get('/filter', 'orderDetailesController@filter')->name('orderDetailes.filter');

    });
    // ************************************** END ORDER DETAILES ROUTES ************************
// ************************************** START ORDERS  ROUTES ************************
Route::prefix('orders')->group(function()
{
    Route::get('index','ordersController@index')->name('orders.index');                                                         // SHOW ALL ORDERS [ORDERS - RETURNS]
    Route::post('changeServant/{id}','ordersController@changeServant')->name('orders.changeServant');                           // CHANGE SERVANT ID IN ORDER TABLE
    Route::post('store','ordersController@store')->name('orders.store');                                                        // CREATE NEW ORDER AND UPDATE ORDER_ID IN ORDER DETAILES TABLE
    Route::get('edit/{id}','ordersController@edit')->name('orders.edit');                                                       // UPDATE ORDER STATUS TO COMPLETED
    Route::get('show/{id}','ordersController@show')->name('orders.show');                                                       // SHOW ITEMS OF ORDER PAGE
    Route::post('changeStatus','ordersController@changeStatusItems')->name('orders.changeStatus');                              // CHANGE STATUS OF ITEMS OF ORDER
    Route::get('softDelete','ordersController@softDelete')->name('orders.softDelete');                                          // TO SHOW ALL ORDER COMBLETED
    Route::get('restore','ordersController@restore')->name('orders.restore');                                                   // RESTORE ORDER TO UNCOMPLEATED ORDER PAGE
    Route::get('show_order_detailes/{id}','ordersController@show_order_detailes')->name('orders.show_order_detailes');          // TO SHOW ORDER DETAILES IN ORDER COMPLEATED PAGE
    Route::post('productNote/{id}','ordersController@productNote')->name('orders.productNote');                                 // UPDATE NOTE TO PRODUCT IN PRODUCTS TABLE AND ORDER DETAILES TABLE
    Route::get('addProduct/{id}','ordersController@addProduct')->name('orders.addProduct.get');                                 // SHOW ADD NEW PRODUCT TO ORDER  PAGE
    Route::post('addProduct','ordersController@StoreProduct')->name('orders.addProduct.post');                                  // UPDATE NEW PRODUCT TO ORDER PAGE
    Route::get('forceDeleteItem/{id}','ordersController@forceDeleteItem')->name('orders.forceDeleteItem');                      // DELETE ITEM FROM ORDER AND RETURN IT TO PRODUCTS TABLE  [AS A ORDER DETAILES]
    // EDIT ORDER DETAILES
    Route::get('editOrderItem/{id}','ordersController@editOrderItem')->name('orders.editOrderItem'); //to show edit item page
    Route::post('updateOrderItem/{id}','ordersController@updateOrderItem')->name('orders.updateOrderItem'); //to update item Detailes
    Route::get('deleteOrder/{id}','ordersController@deleteOrder')->name('orders.deleteOrder'); //to update item Detailes
     Route::post('profit/','ordersController@profit')->name('orders.profit');                                                    //CREATE COMPANY
       Route::post('restoreReturns','ordersController@restoreReturns')->name('orders.restoreReturns');                         //RESTORE RETURNS ITEMS

    // PROFIT

});
// ************************************** END ORDERS ROUTES ************************
// ************************************** START RETURNS  ROUTES ************************
Route::prefix('returns')->group(function()
{
    Route::get('index','ReturnsController@index')->name('returns.index');
    Route::get('softDelete','ReturnsController@softDelete')->name('returns.softDelete');
    Route::get('restore','ReturnsController@restore')->name('returns.restore');
});
// ************************************** END RETURNS ROUTES ************************
    // ************************************** START REBORTS ROUTES ************************



    Route::prefix('reborts')->group(function()
    {
        Route::get('index','RebortesController@index')->name('reborts.index');

        Route::get('allProducts','RebortesController@allProducts')->name('reborts.allProducts');
        Route::post('/add/day','RebortesController@setday');
        Route::post('/add/dayy1','RebortesController@oneday');
        // SERVANTS REBORTS
        Route::get('servantindex','RebortesController@servantindex')->name('reborts.servantindex');
        Route::post('/add/day1','RebortesController@servantname')->name('servant');
        Route::get('/showMore/{id}','RebortesController@showMore')->name('reborts.showMore');
        // SUPPLIER REBORTS
        Route::get('/getCastomer_index','RebortesController@getCastomer_index')->name('reborts.castomerIndex');
        Route::post('/getCastomer_reborts','RebortesController@getCastomer_reborts')->name('reborts.getCastomer_reborts');

        Route::get('/orderNumber_index','RebortesController@orderNumber_index')->name('reborts.orderNumber_index');
        Route::post('/orderNumber_reborts','RebortesController@orderNumber_reborts')->name('reborts.orderNumber_reborts');

        Route::get('/completeProduct/{id}','RebortesController@completeProduct')->name('reborts.completeProduct');
    });

    // ************************************** END REBORTS ROUTES ************************


});


Route::namespace('Admin')->middleware('guest:admin')->group(function()
    {
        Route::get('/login','LoginController@loginForm')->name('admin.login');
        Route::post('/makelogin','LoginController@login')->name('admin.MakeLogin');
    });
});







