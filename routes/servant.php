<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Servant Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::prefix('servant')->namespace('Servant')->middleware('guest:servant')->group(function()
{
    // FORGET PASSWORD 
    Route::get('login','homeController@login')->name('servant.login');
    Route::post('makeLogin','homeController@makeLogin')->name('servant.makeLogin');
   
   
});

Route::prefix('servant')->namespace('Servant')->middleware('auth:servant')->group(function()
{
    Route::get('Dashboard','homeController@home')->name('servant.home');
    Route::get('allOrders','homeController@allOrders')->name('servant.allOrders');
    Route::get('showOrderDetailes/{id}','homeController@showOrderDetailes')->name('servant.showOrderDetailes');
    Route::get('logout','homeController@logout')->name('servant.logout');
});

