@extends('user.layouts.app')
@section('content')
<div id="contact" class="section wb">
    <div class="container">
        <div class="section-title text-center">
            <h3>استعادة كلمة المرور</h3>
        </div><!-- end title -->

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="contact_form">
                    <div id="message">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('admin.alerts.success')
									@include('admin.alerts.errors')
                    </div>

                    <form  class="row" action="{{ route('forgetPassword.post.User') }}"  method="post" style="direction: rtl">
                        @csrf

                        <fieldset class="row-fluid">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="email" name="email" class="form-control" placeholder="ادخل ايميل المستخدم">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                         
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center" style="direction: rtl !important">
                                <input type="submit" style="direction: rtl" value="ارسال" class="btn btn-light btn-radius btn-brd grd1 btn-block">
                            </div>
                        </fieldset>
                    </form>
                    
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
        
       
        
    </div><!-- end container -->
</div>
@endsection