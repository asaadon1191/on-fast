@extends('user.layouts.app')

@section('content')
<br><br><br>
<div class="content-6-mian py-5">
    <div class="container py-lg-5 py-md-4">
      <div class="content-info-in row">
        <div class="content-gd col-lg-6 pr-lg-4">
          <div class="title-content text-left mb-sm-4 mb-3">
            <h3 class="subhny-title">About Us</h3>
            <h3 class="hny-title">Hello. Our structure has been present for over 20 years. We make the best !.</h3>
          </div>
          <p>Lorem ipsum dolor sit amet, suscipit totam animi consequatur saepe Ea consequuntur
            .Ea consequuntur illum facere aperiam sequi optio consectetur adipisicing elit fuga, Sed ac fringilla ex.
            Nam mauris velit, ac cursus quis, non leo placerat libero non, dapibus leo. </p>
        </div>
        <div class="content-gd col-lg-3 col-6 pl-lg-4 pl-lg-5 mt-lg-0 mt-md-5 mt-4">
          <img src="assets/user/assets/images/crew-Copy.png" alt="about" class="radius-image img-fluid">
        </div>
        <div class="content-gd col-lg-3 col-6 pl-lg-4 pl-lg-5 mt-lg-0 mt-md-5 mt-4">
          <img src="assets/user/assets/images/business-3167295_1920.jpg" alt="about" class="radius-image img-fluid">
        </div>
      </div>
      <p class="mt-4">Lorem ipsum dolor sit amet, suscipit totam animi consequatur saepe Ea consequuntur
        .Ea consequuntur illum facere aperiam sequi optio consectetur adipisicing elit fuga, Sed ac fringilla ex.
        Nam mauris velit, ac cursus quis, non leo placerat libero non, dapibus leo.Lorem ipsum dolor sit amet, suscipit totam animi consequatur saepe Ea consequuntur .Ea consequuntur illum facere aperiam sequi optio consectetur adipisicing elit fuga, Sed ac fringilla ex. Nam mauris velit, ac cursus quis, non leo placerat libero non, dapibus leo. </p>
    </div>
</div><br><br>

<section class="w3l-grids-3 py-5">
    <div class="container py-lg-5 py-md-4">
        <div class="row bottom-ab-grids">
          <div class="col-lg-6 bottom-right-grids mb-lg-0 mb-5">
            <img src="assets/images/pic5.png" alt="" class="img-fluid">
           </div>
            <div class="col-lg-6 bottom-ab-left pl-lg-5">
                <span class="subhny-title mb-2">Faster than light</span>
                <h3 class="hny-title mb-4">Why Choose Us</h3>
                <p class="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur hic odio
                    voluptatem
                    tenetur consequatur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                    cubilia. </p>
                <ul class="cont-4 mt-4">
                    <li><span class="fa fa-check"></span>Pharetra massa init ultricies labore dolor amet</li>
                    <li><span class="fa fa-check"></span>Dolore magna aliqua init sodales tempor</li>
                    <li><span class="fa fa-check"></span>Incididunt ut labore et, pharetra massa</li>
                </ul>
                <a href="about.html" class="btn btn-style btn-primary mt-lg-5 mt-4">Read More <span class="fa fa-chevron-right"></span></a>
            </div>

        </div>
    </div>
</section>
@endsection
