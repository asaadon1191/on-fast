@extends('user.layouts.app')
@section('content')
    <div id="contact" class="section wb">
        <div class="container">
            <div class="section-title text-center">

               <div id="flashMessages">
                    @include('admin.alerts.success')
                    @include('admin.alerts.errors')
               </div>

                <h3>تعديل الملف الشخصي</h3>
            </div><!-- end title -->

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="contact_form">
                        <div id="message"></div>
                        <form  class="row" action="{{ route('user.profile.update') }}" name="contactform" method="post" style="direction: rtl">
                            @csrf
                            <fieldset class="row-fluid">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="name"  class="form-control" placeholder=" اسم المستخدم" value="{{ auth()->user()->name }}">
                                    @error("name")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="email"  class="form-control" placeholder=" البريد الالكتروني للمستخدم" value="{{ auth()->user()->email }}">
                                    @error("email")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="phone"  class="form-control" placeholder="  تليفون المستخدم" value="{{ auth()->user()->phone }}">
                                    @error("phone")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="password"  class="form-control" placeholder="   كلمة المرور">
                                    @error("password")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="password_confirmation"  class="form-control" placeholder="  تاكيد كلمة المرور ">
                                </div>
                               
                               
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center" >
                                    <input type="submit" value="تعديل" class="btn btn-light btn-radius btn-brd grd1 btn-block">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
            
          
            
        </div><!-- end container -->
    </div>
@endsection
@section('js')
    <script>
        $("#flashMessages").fadeOut(3000);
    </script>
@endsection