
@extends('user.layouts.app')

@section('content')
<br><br><br><br><br>
<div class="contant11-top-bg">
    <div class="container">
        <span class="subhny-title text-center mb-2">تواصل معنا</span>
        <h3 class="hny-title text-center mb-md-5 mb-4">تواصل معنا عن طريق</h3>
        <div class="d-grid contact section-gap">
            <div class="contact-info-left d-grid text-center">
                <div class="contact-info">
                    <span class="fa fa-map-marker" aria-hidden="true"></span>
                    <h4>Location</h4>
                    <p>Dolor sit, #PTH,8800 Honey Street UK.</p>
                </div>
                <div class="contact-info">
                    <span class="fa fa-phone" aria-hidden="true"></span>
                    <h4>Phone</h4>
                    <p><a href="tel:+44 7834 857829">+22 123 984 434</a></p>
                    <p><a href="tel:+44 987 654 321">+44 123 984 439</a></p>
                </div>
                <div class="contact-info">
                    <span class="fa fa-envelope" aria-hidden="true"></span>
                    <h4>Email</h4>
                    <p><a href="mailto:mail@example.com" class="email">mail@example.com</a></p>
                    <p><a href="mailto:mail@example1.com" class="email">mail@example1.com</a></p>
                </div>
                <div class="contact-info">
                    <span class="fa fa-clock-o" aria-hidden="true"></span>
                    <h4>Working Hours</h4>
                    <p>Wednesday - Sunday</p>
                    <p>7:00 AM - 9:00 PM</p>
                </div>
            </div>
        </div>
    </div>
</div><br><br>br>
@endsection


