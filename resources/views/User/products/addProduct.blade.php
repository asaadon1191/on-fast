@extends('user.layouts.app')

@section('content')


    <div class="container" style="margin-top: 200px; margin-bottom:100px">

        <div class="text-center mb-5" >
            <h1>
            اضافة شحنة جديدة
            </h1>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="contact_form">
                    <div id="message">
                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif

                        <div id="success">
                            @include('admin.alerts.success')
                        </div>
                        <div  id="error">
                            @include('admin.alerts.errors')
                        </div>
                    </div>

                    <form  class="row" action="{{ route('store.create.product') }}"  method="post" style="direction: rtl">
                        @csrf


                        <fieldset class="row-fluid">

                             <!-- RESEVER NAME  -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="resever_name" class="form-control" placeholder="ادخل اسم المستلم">
                                @error('resever_name')
                                <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                @enderror


                            </div>

                             <!-- RESEVER PHONE  -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="number" name="resver_phone" class="form-control" placeholder="ادخل تليفون المستلم">
                                @error('resver_phone')
                                    <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                @enderror
                            </div>

                             <!-- GOV  NAME  -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="float:right">
                                <select  class="form-control" id="gov">
                                    <option value="">اختار محافظة</option>
                                    @foreach ($governorates as $gov)
                                        <option value="{{ $gov->id }}">
                                            {{ $gov->name }}
                                        </option>
                                    @endforeach
                                </select>

                            </div>

                              <!-- CITY  NAME  -->
                              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="float:left">
                                <select class="search_form_select form-control" name="city_id" id="city">
                                    <option disabled selected>اختار المدينة </option>
                                </select>

                                @error('city_id')
                                    <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                @enderror

                            </div>



                              <!-- ADRESS  -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="adress" class="form-control" placeholder="ادخل عنوان المستلم">
                                @error('adress')
                                    <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                @enderror
                            </div>


                              <!-- PRODUCT PRICE  -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="float:right">
                                <input type="text" name="product_price" class="form-control" placeholder="ادخل سعر الشحنة"  id="productPrice" >
                                @error('product_price')
                                <span class="invalid-feedback" role="alert">
                                    <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                </span>
                                @enderror
                            </div>

                              <!-- SHIPPING PRICE  -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="float:left">
                                <input type="text" name="shipping_price" class="form-control" placeholder="ادخل سعر الشحن"  id="shippingPrice">
                                @error('shipping_price')
                                <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                @enderror
                            </div>

                              <!-- TOTAL PRICE  -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="total_price" class="form-control" placeholder="الاجمالي"  id="totalPrice">
                                @error('total_price')
                                    <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                @enderror
                            </div>

                                <!-- NOTES  -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="">الملاحظات</label>
                                <textarea name="notes" id="" cols="81" rows="3">

                                </textarea>
                            </div>

                             <!-- RESEVER DATE  -->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="">تاريخ التسليم</label>
                                    <input type="date" name="rescive_date" class="form-control">
                                    @error('rescive_date')
                                    <div class="alert alert-danger text-center rounded" style="width: 50%; padding: 3px; margin-top: -4%; background-color: #f8c701" >{{ $message }}</div>
                                    @enderror
                                </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center" style="direction: rtl !important">
                                <input type="submit" style="direction: rtl" value="حفظ" class="btn btn-light btn-radius btn-brd grd1 btn-block">
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div>
@endsection

@section('js')

    {{--  GET CITIES  --}}
    <script>
        $(document).ready(function()
        {
            $('#gov').on('change',function()
            {
                var gov = $(this).val();

                if(gov)
                {
                    $.ajax(
                        {
                            url:"{{ url('/users/cities/') }}/" + gov,
                            type:"GET",
                            dataType:"json",
                            success:function(data)
                            {
                                $("#city").empty();
                                $.each(data,function(key,value)
                                {
                                    $("#city").append('<option value="'+value.id+'">'+value.name+'</option>')
                                });
                            }
                        });
                }else
                {
                    alert('Error');
                }
            });
        });
    </script>



    {{-- GET PRODUCT PRICE --}}
        <script>

            $(document).ready(function()
            {
                $('#productPrice').on('change',function()
                {
                    var product_price   = $("#productPrice").val();
                    var shipping_price  = $("#shippingPrice").val();
                    var total_price     = $("#totalPrice").val(product_price + shipping_price);
                });
                $('#shippingPrice').on('change',function()
                {
                    var product_price2   = $("#productPrice").val();
                    var shipping_price2  = $("#shippingPrice").val();
                    var total_price2     = $("#totalPrice").val(parseInt(product_price2) +parseInt(shipping_price2));
                });
            });
        </script>


@endsection
