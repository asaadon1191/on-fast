
    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
			<div class="loader__bar"></div>
			<div class="loader__bar"></div>
			<div class="loader__bar"></div>
			<div class="loader__bar"></div>
			<div class="loader__bar"></div>
			<div class="loader__ball"></div>
		</div>
    </div><!-- end loader -->
    <!-- END LOADER -->

	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="left-top">
						<div class="email-box">
							<a href="https://m.facebook.com/onfastdelivery/?_rdr"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li> OnFast.com</a>
						</div>
						<div class="phone-box">
							<a href="tel:040-3409968"><i class="fa fa-phone" aria-hidden="true"></i> 040-3409968</a>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6">
					<div class="right-top">
						<div class="social-box">
							<ul>
								<li><a href="https://m.facebook.com/onfastdelivery/?_rdr"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
							<ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <header class="header header_style_01">
        <nav class="megamenu navbar navbar-default">
            @auth
			<div class="container-fluid">
                <div class="navbar-header navbar-right">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('assets/admin/images/ca297136-78d1-422c-8274-0afa15d1b748.jpg') }}" alt="image" style="height: 80px; width:80px; border-radius: 50%; color:orange"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-left">



                        <li>
							<a href="{{ route('user.logout') }}">تسجيل الخروج</a>
						</li>
                        <li><a href="{{ route('user.profile') }}"> الملف الشخصي</a></li>
                        <li><a href="{{ route('user.index.product') }}">الشحنات الجديدة</a></li>
                        <li><a  href="{{ route('home') }}">الصفحة الرئيسية</a></li>
                    </ul>
                </div>
            </div>
			@else
				<div class="container-fluid">
					<div class="navbar-header navbar-right">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="{{ route('welcome') }}"><img src="{{ asset('assets/admin/images/ca297136-78d1-422c-8274-0afa15d1b748.jpg') }}" alt="image" style="height: 80px; width:80px; border-radius: 50%; color:orange"></a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-left">



							<li><a href="{{ route('user.contactUs') }}">تواصل معنا</a></li>
							<li><a href="{{ route('login') }}">تسجيل الدخول</a></li>
							<li><a href="{{ route('register') }}">حساب جديد</a></li>
							<li><a href="about-us.html">خدماتنا</a></li>
						</ul>
					</div>
				</div>
			@endauth
        </nav>
    </header>


