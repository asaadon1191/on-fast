<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-sm-10 col-md-10 col-sm-offset-1 pd-add">
                <div class="address-item" >
                    <div class="address-icon"  >
                        <i class="icon icon-location2" style="color: #fff"></i>
                    </div>
                    <h3>الغربية - طنطا</h3>
                    <p>PO Box 16122 Collins Street West
                        <br> Victoria 8007 Australia</p>
                </div>
                <div class="address-item">
                    <div class="address-icon">
                        <i class="fa fa-envelope" aria-hidden="true" style="color: #fff"></i>
                    </div>
                    <h3>تواصل  عبر الايميل</h3>
                    <p>on-fast@gmail.com</p>
                </div>
                <div class="address-item">
                    <div class="address-icon">
                        <i class="icon icon-headphones" style="color: #fff"></i>
                    </div>
                    <h3>تواصل معنا </h3>
                    <p><i class="icon icon-whatsapp"></i>  015-515-94028
                        <br><i class="fa fa-phone" aria-hidden="true"></i>  010-143-34643</p>
                </div>
            </div>
        </div>
    </div><!-- end container -->
</footer><!-- end footer -->

<div class="copyrights">
    <div class="container">
        <div class="footer-distributed">
            <div class="footer-left">
                <p class="footer-company-name">All Rights Reserved. &copy; 2020 <a href="#">Asaadon</a> Design By : </p>
            </div>


        </div>
    </div><!-- end container -->
</div><!-- end copyrights -->

<a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

<!-- ALL JS FILES -->
<script src="{{ asset('assets/user/js/all.js') }}"></script>
<!-- ALL PLUGINS -->
<script src="{{ asset('assets/user/js/custom.js') }}"></script>
<script src="{{ asset('assets/user/js/portfolio.js') }}"></script>
<script src="{{ asset('assets/user/js/hoverdir.js') }}"></script>

{{-- CHANGE CLASS ACTIVE IN NAVBAR  --}}
<script>
    const currentLocation   = location.href;
    const menu_li           = document.querySelectorAll('#navbar li a');
    const menu_length       = menu_li.length;
    for (let index = 0; index < menu_length; index++)
    {
        // alert(menu_li[index].href)
        if (menu_li[index].href === currentLocation)
        {
            menu_li[index].className = "active";
        }
    }
</script>
@yield('js')

</body>
</html>
