@extends(' user.layouts.app')

@section('content')

    <style>
        .card
        {
            margin-top:-5%
        }
        .color
        {
            color: #F7B614;
            font-size: 25px
        }
        .search
        {
            margin-top: 3%
        }
        #val::placeholder
        {
            color: #fff;
        }
    </style>



 {{-- SEARCH FORM  --}}
   <div class="container search" style="width: 50%">

    <form id="searchForm">
        <div class="form-group">
            <input type="text" name="filter" class="form-control text-center" placeholder="بحث برقم تليفون العميل او اسم العميل " id="val" style="background-color: #a1a1a1; color:#fff">
            <span class="text-danger" id="search_error"></span>
        </div>
        {{-- <input type="submit" value="بحث" id="search" class="form-control btn btn-warning"> --}}
    </form>
   </div>


    <div class="container test">
            @if ( $productCompleted && $productCompleted->count() == 0)
                <h2 class="text-center" style="margin-top: 5%">
                    لا يوجد شحنات
                </h2>
            @else

                {{-- SEARCH RESULTS  --}}


                    @if ($productCompleted && $productCompleted->count() > 0)




                        {{-- *******************************************************************************  --}}

                        <div id="about" class="section wb">
                            <div class="container">
                                <div class="section-title text-center">
                                    <h3>كل الشحنات</h3>
                                </div><!-- end title -->


                                {{-- START RESULTS --}}
                                    <div class="row" id="results">

                                    </div>
                                {{-- END RESULTS --}}

                                <div class="row" id="products">
                                    @foreach ($productCompleted as $return)
                                    {{-- {{$return}} --}}
                                        <div class="col-md-4 col-sm-6" style="margin-bottom:  5%" id="products">
                                            <div class="pricingTable">
                                                <div class="pricingTable-header" style="direction: rtl">
                                                    <h3 class="title">{{ $return->resever_name }}</h3>
                                                    <span class="sub-title">{{ $return->resver_phone }}</span>
                                                    @if ($return->type == 4)
                                                        <span class="year"><span style="color: #000">حالة الشحنة</span> <br>في انتظار الموافقة</span>
                                                    @else
                                                        <span class="year"><span style="color: #000">حالة الشحنة</span>  <br>{{ $return->status->name }}</span>
                                                    @endif
                                                </div>
                                                <div class="price-value">
                                                    <div class="value">
                                                        <span class="currency">جنيه</span>
                                                        <span class="amount">{{ $return->total_price }}</span>
                                                        <span class="month">الاجمالي</span>
                                                    </div>
                                                </div>
                                                <ul class="pricing-content">
                                                    <li><strong><span style="color: orange">المحافظة : </span>{{ $return->cities->governorate->name }}</strong></li>
                                                    <li><strong><span style="color: orange">المدينة : </span>{{ $return->cities->name }}</strong></li>
                                                    <li><strong><span style="color: orange">سعر الشحنة : </span>{{ $return->product_price }}</strong></li>
                                                    <li><strong><span style="color: orange">قيمة الشحن : </span>{{ $return->shipping_price }}</strong></li>
                                                    <li><strong><span style="color: orange">تاريخ التسجيل : </span>{{ \Carbon\Carbon::parse($return->created_at)->diffForhumans() }}</strong></li>
                                                </ul>
                                                <a href="{{ route('user.packageDetailes',$return->id) }}" class="pricingTable-signup">عرض التفاصيل</a>
                                            </div>
                                        </div>
                                    @endforeach

                                </div><!-- end row -->
                            </div><!-- end container -->
                        </div>

                    <div class="mt-3" id="paginate">
                        {{ $productCompleted->links() }}
                    </div>
                    @endif

            @endif
    </div>
 <br><br><br>
@endsection

@section('js')
    {{-- FILTER FORM  --}}
    <script>
        $(document).on('keyup','#val',function(e)
        {
            e.preventDefault();

            // DELETE ERROR MESSAGE IF INPUT HAVE VALUE WITHOUT REFRESH PAGE
			$('#search_error').text('');

            //Get Form Data
			var formData = new FormData($('#searchForm')[0]);

            var q       = $(this).val();
            // alert(q);

           if (q)
           {
            $.ajax(
			{
                url: "{{route('user.filter')}}",
				type: 'GET',
				data:
                {
                    'filter' : q,
                },

				success: function(data)
                {
                    $("#results").empty();
                    $("#products").hide();
                    $("#paginate").hide();
                   if (data.dataa && data.dataa.length != 0 && q != '')
                   {
                        $.each(data.dataa,function(key,value)
                        {
                            $("#results").append
                                (
                                    '<div style="margin-top:10%" class="col-md-4">'+
                                        '<div class="card">'+
                                            '<div class="card-header text-center" style="font-weight:600">'+
                                                '<span class="color">'+
                                                    'رقم الشحنة :'
                                                +'</span>'
                                                +'<span>'+
                                                    value.package_number+
                                                '</span>'+
                                            '</div>'+

                                            '<ul class="list-group list-group-flush">'+

                                                '<li class="list-group-item">'+
                                                    '<span class="color">'+
                                                        'حالة الشحنة :'
                                                    +'</span>'+
                                                    '<span>'+
                                                        value.status.name
                                                    +'</span>'+
                                                '</li>'+

                                                '<li class="list-group-item">'+
                                                    '<span class="color">'+
                                                        'اسم المورد :'
                                                    +'</span>'+
                                                    '<span>'+
                                                        value.supplier.name
                                                    +'</span>'+
                                                '</li>'+

                                                '<li class="list-group-item">'+
                                                    '<span class="color">'+
                                                        'اسم العميل :'
                                                    +'</span>'+
                                                    '<span>'+
                                                        value.resever_name
                                                    +'</span>'+
                                                '</li>'+

                                                '<li class="list-group-item">'+
                                                    '<span class="color">'+
                                                        'تاريخ التوصيل :'
                                                    +'</span>'+
                                                    '<span>'+
                                                        value.rescive_date
                                                    +'</span>'+
                                                '</li>'+

                                                '<li class="list-group-item">'+
                                                    '<span class="color">'+
                                                        ' سعر الشحنة :'
                                                    +'</span>'+
                                                    '<span>'+
                                                        value.product_price
                                                    +'</span>'+
                                                '</li>'+

                                                '<li class="text-center">'+
                                                    '<a href="{{ url('/users/packageDetailes/') }}/'+value.id+'" class="btn btn-warning">'+
                                                        'عرض المزيد'
                                                    +'</a>'+
                                                '</li>'+
                                            '</ul>'+
                                        '</div>'+
                                    '</div>'
                                )

                        });
                   }else
                   {
                        $("#results").append
                        (
                            '<h2 class="text-center" style="margin-top:20px">'+
                                'لا يوجد شحنات'
                            +'</h2>'
                        )
                   }
                }
            });
           }
        });
    </script>

 {{-- SHOW ALL DATA IF FILTER INPUT == '' --}}
    <script>
         $(document).on('keyup','#val', function(e)
         {
            e.preventDefault();
            var q = $('#val').val();

            if (q == '')
            {
                $("#results").empty();
                $("#products").show();
                $("#paginate").show();
                $("#orderDEtailes").show();
            }
         });
    </script>

@endsection

