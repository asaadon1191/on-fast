<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\permissionRequest;

class PermissionsController extends Controller
{
    public function index()
    {
        $permissions = Permission::with('roles')->get();
        return view('admin.permissions.index',compact('permissions'));
    }



    public function store(permissionRequest $request)
    {
       try
       {
           DB::beginTransaction();
                $create = Permission::create(
                    [
                        'name'          => $request->name,

                    ]);


            DB::commit();


            if($create)
            {

                return response()->json(
                    [
                        'status' => true,
                        'msg' => 'تم الحفظ بنجاح',
                        'dataa' => $create
                    ]);
            }else
            {
                return response()->json(
                    [
                        'error' => 'هناك خطا ما برجاءالمحاولة فيما بعد'
                    ]);
            }
       } catch (\Throwable $th)
       {
           return $th;
           DB::rollback();
            return redirect()->route('permissions.index');
       }
    }

    public function edit($id)
    {
        try
       {
           $permission = Permission::find($id);


           return view('admin.permissions.edit',compact('permission'));

       } catch (\Throwable $th)
       {
           return $th;
            return redirect()->route('permissions.index')->with(['error' => 'هناك خطا ما برجاء المحاولة فيما بعد']);
       }
    }

    public function update($id,permissionRequest $request)
    {
        // return $request;
        try
       {
            $permission = Permission::find($id);


            DB::beginTransaction();
                $update = $permission->update(
                    [
                        'name'          => $request->name,
                    ]);

            DB::commit();

            return redirect()->route('permissions.index')->with(['success' => 'تم التعديل بنجاح']);

       } catch (\Throwable $th)
       {
           return $th;
            return redirect()->route('permissions.index')->with(['error' => 'هناك خطا ما برجاء المحاولة فيما بعد']);;
       }
    }

    public function distroy($id)
    {

    }
}
