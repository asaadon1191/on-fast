<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $supplier = Supplier::where('phone',auth()->user()->phone)->first();

        if ($supplier == '')
        {
            $productCompleted = '';
            return view('home',compact('productCompleted'));
        } else
        {
            $productCompleted = Product::where('supplier_id',$supplier->id)->with('orders_detailes')->whereBetween('created_at',array('2021-11-01', Carbon::now()))->orderByDesc('id')->paginate(10);
            return view('home',compact('productCompleted'));
        }
    }
}
