<?php

namespace App\Http\Controllers\UserS;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;


class forgetPasswordController extends Controller
{
    public function forgetPasswordUser()
    {
        return view('user.password.forgetPassword');
    }
    
    

    public function sendEmail(Request $request)
    {
        $to = 'asaadon1234@yahoo.com'; // note the comma

// Subject
$subject = 'New Password';

// Message
$message = 'Forget Password';
$messagesss = '
<html>

</html>
';

// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

// Additional headers
$headers[] = 'To: ahmed saadon <asaadon1234@yahoo.com>';
$headers[] = 'From: On Fast <support@on-fast.com>';
$headers[] = 'Cc: birthdayarchive@example.com';
$headers[] = 'Bcc: birthdaycheck@example.com';
// return $headers;

// Mail it
        $mail = mail($to, $subject, $message, implode("\r\n", $headers));
        // return mail($to, $subject, $message, implode("\r\n", $headers));
        if($mail)
        {
            return "yes";
        }else
        {
            return "no";
        }
    }

    public function showResetPasswordForm($token) 
    { 
       return view('user.password.changePassword', ['token' => $token]);
    }

    public function submitResetPasswordForm(Request $request)
    {
        $request->validate(
        [
            'email'                   => 'required|email|exists:users',
            'password'                => 'required|string|min:6|confirmed',
            'password_confirmation'   => 'required'
        ]);

        $updatePassword = DB::table('password_resets')->where(
                        [
                            'email' => $request->email, 
                            'token' => $request->token
                        ])->first();

        if(!$updatePassword)
        {
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $user = User::where('email', $request->email)->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email'=> $request->email])->delete();

        return redirect()->route('loginUser')->with('message', 'Your password has been changed!');
    }
}
